import lume from "lume/mod.ts"
import sass from "lume/plugins/sass.ts"
import mdx from "lume/plugins/mdx.ts"
import jsx from "lume/plugins/jsx_preact.ts"
import $ from "dax"
import { ensureDir } from "https://deno.land/std@0.171.0/fs/mod.ts"

export async function ensureSymlink(src: string | URL, dest: string | URL) {
  try {
    await Deno.symlink(src, dest);
  } catch (error) {
    if (!(error instanceof Deno.errors.AlreadyExists)) {
      throw error;
    }
  }
}

async function build() {
  await ensureSymlink('../node_modules', '_site/node_modules')
  await ensureDir('_site/syntax')
  await $`aseprite -b --split-slices syntax.aseprite --save-as '_site/syntax/{slice}.png'`
}

import remarkSmartypants from 'npm:remark-smartypants'
import remarkMath from 'npm:remark-math'
import rehypeKatex from 'npm:rehype-katex'

const site = lume()
site.ignore("readme.md")
site.copy("git.png")
site.use(jsx())
site.use(mdx({
  remarkPlugins: [
    remarkMath,
    remarkSmartypants,
  ],
  rehypePlugins: [
    rehypeKatex,
  ]
}))
site.use(sass())
site.addEventListener("afterBuild", async (_event) => {
  await build()
})
site.addEventListener("afterUpdate", async (event) => {
  if (event.files && event.files.has("/syntax.aseprite")) {
    await build()
  }
})
export default site
