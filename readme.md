## install

- install `deno`
- install `npm` (or an alternative)
- install `aseprite`

```
pnpm install # or 'npm install'
deno task serve

node render.js # render PDF
```

## TODO

- finish algebraic syntax??
- peer review ?
