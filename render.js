import puppeteer from "puppeteer-core"
import * as fs from "node:fs/promises"

const browser = await puppeteer.launch({
  product: "chrome",
  executablePath: "chromium",
  headless: true,
})
const page = await browser.newPage()
await page.goto("http://localhost:3000/")

const pdfPath = "render.pdf"
try {
    await fs.unlink(pdfPath)
} catch {
  // do nothing
}
await page.pdf({
  path: pdfPath,
  format: "A4",
})
await page.close()
await browser.close()
