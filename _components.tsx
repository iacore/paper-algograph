// @ts-ignore:
export function C({ children }) {
  assert(typeof children === "string")
  return (
    <sub class="bibref">
      <a href={"#" + children}>[{children}]</a>
    </sub>
  )
}
import { assert } from "https://deno.land/std@0.177.0/testing/asserts.ts"
import { Fragment } from "npm:preact/jsx-runtime"
// @ts-ignore:
export function Cref({ children }) {
  assert(typeof children === "string")
  return (
    <span id={children} class="bib">
      [{children}]
    </span>
  )
}

import { unified } from "npm:unified"
import remarkParse from "npm:remark-parse"
import remarkRehype from "npm:remark-rehype"
import remarkGfm from 'npm:remark-gfm@3'
import remarkSmartypants from 'npm:remark-smartypants'
import remarkMath from 'npm:remark-math'
import rehypeKatex from 'npm:rehype-katex'
import rehypeStringify from "npm:rehype-stringify"
const remarkPipeline = unified()
  .use(remarkParse) // Parse markdown content to a syntax tree
  .use(remarkMath)
  .use(remarkSmartypants)
  .use(remarkGfm)
  .use(remarkRehype, {allowDangerousHtml: true}) // Turn markdown syntax tree to HTML syntax tree, ignoring embedded HTML
  .use(rehypeKatex)
  .use(rehypeStringify, {allowDangerousHtml: true}) // Serialize HTML syntax tree

export function toMarkdown(content: string, stripParagraph?: boolean) {
  const __html = String(remarkPipeline.processSync(content));
  if (!stripParagraph) return __html
  const match = /<p>(.*)<\/p>/.exec(__html)
  assert(match, "assume outer <p>")
  return match[1]
}

type SyntaxRule = {
  src: string,
  title: string,
  desc: string,
}

export function EffectTable(syntax_rules: SyntaxRule[]) {
  return (
    <table class="syntax-container">
      {syntax_rules.map(item =>
      <tr class="syntax">
        <td class="image">
          <img src={"syntax/" + item.src + ".png"}/>
        </td>
        <td class="text">
          <h3 class="title" dangerouslySetInnerHTML={{ __html: toMarkdown(item.title, true) }}/>
          <div class="desc" dangerouslySetInnerHTML={{ __html: toMarkdown(item.desc) }}/>
        </td>
      </tr>)}
    </table>
  )
}
