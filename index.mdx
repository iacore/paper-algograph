---
layout: layout.njk
title: algograph
---

import { C, Cref, toMarkdown, EffectTable } from './_components.tsx'

# A Drawn Programming Language Based on Calculus of Constructions and Algebraic Effects [![View the source of this repo here](git.png)](https://git.envs.net/iacore/paper-algograph.git)

<div class="h1-info">

Locria Cyber  
`i+research@insec.net`

I present *algograph*, a graphical notation to denote algorithms. It is designed to be drawn by hand quickly and to aid type checking with clear visuals. Its inspiration and its syntax are explained below.

</div>

## Acknowledgements

Algograph was invented to write algorithms faster than typing on a keyboard.
It started as a drawn syntax of Koka<C>koka</C>. Then, syntax for a type system (based on Calculus of Constructions<C>coc</C>) is added.

The "effect handlers" part of algograph is closer to linear types in Idris<C>idris</C>, ATS<C>ats</C>, or Clean<C>clean</C> than the row-based effect handler<C>eff</C> in Koka. You can treat them as data values that cannot be copied, just like in Idris.

Some syntax, like the triangle shape of inductive type constructors, is borrowed from interaction nets<C>intr</C>. Interaction nets have no semantic influence on this language.

The "dependent type" part of this language is inspired by Idris.

## Introduction

Algograph is a set of graphical notations used to denote algorithms. It is designed to be drawn quickly on paper. It can be used to write mathematical proofs and algorithms. 
Unlike text-based programming languages, algograph is not bound by a particular text encoding scheme. Since the language is drawn, terse writing systems, like Hanzi, can be used to symbolize code blocks in limited space.

The syntax described in this article is limited. The user is advised to use their native writing system, and invent new syntax and shorthand to accelerate authoring algorithms.

## Base Syntax

{EffectTable(baseSyntax)}

## Syntax Extension : Algebraic Effect

Because of the language's graph-like nature, algorithms in algograph are not bound by the linearity of text. By default, the language has the weakest ordering constraint for operations. An implementation can choose to parallelize the execution of causally independent parts of the program. Therefore, causal ordering needs to be explicitly stated.

What "effect" means in an algograph drawing depends on the context. Examples of stack monads (similar to effects in Koka) and linear values (terms that cannot be copied) are provided below.

{EffectTable(effectSyntax)}

{/*## Effect Entanglement Type

Formal system for entanglement type. What is valid, what is not.

Rewrite system, simplify.

TK*/}

## References

<div class="bibliography">

<Cref>coc</Cref> T. Coquand, Gérard Huet. The calculus of constructions. RR-0530, INRIA. 1986. inria-00076024  
<Cref>intr</Cref> Lafont, Yves. (1990). Interaction nets. Proceedings of the 17th ACM SIGPLAN-SIGACT Symposium on Principles of Programming Languages - POPL ’90. doi:10.1145/96709.96718  
<Cref>eff</Cref> Xie, Ningning and Leijen, Daan. (2021). Generalized Evidence Passing for Effect Handlers (or, Efficient Compilation of Effect Handlers to C) (Extended Version). [MSR-TR-2021-5](https://www.microsoft.com/en-us/research/publication/generalized-evidence-passing-for-effect-handlers/)  

<Cref>ats</Cref> ATS: https://www.cs.bu.edu/~hwxi/atslangweb/  
<Cref>clean</Cref> Clean: https://wiki.clean.cs.ru.nl/Clean  
<Cref>idris</Cref> Idris 2: https://idris-lang.org/ Source: https://github.com/idris-lang/Idris2/  
<Cref>koka</Cref> Koka: https://koka-lang.github.io/koka/doc/index.html Source: https://github.com/koka-lang/koka  

</div>
